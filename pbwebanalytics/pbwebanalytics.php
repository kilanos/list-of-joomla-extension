﻿<?php
/**
 * @version    	$version 3.0 karlo  $
 * @copyright	Copyright (C) 2012 PB Web Development. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Updated    	November 2013
 * lacking footer display and meta hook
 */
 
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

//class naming convention is important
class plgSystemPbwebAnalytics extends JPlugin
{
	
	function pglPbwebAnalytics( &$subject, $params )
	{
		parent::__construct($subject, $config);		
	}
		
	function onAfterRender()
	{
		$trackerCode = $this->params->get('code', '');
		$display = $this->params->get('display', '');		        
				
		$page = JFactory::getApplication();

        // skip if admin page
        if ($page->isAdmin()) 
		{		
			return true;
        }
		
		$analytics_js  = "\n\t\t <!-- Start Google Analytics Plugin -->\n";
		$analytics_js .= "\n\t\t  var _gaq = _gaq || []; \n";
		$analytics_js .= "\t\t _gaq.push(['_setAccount', '" . $trackerCode . "']);\n\n";
		$analytics_js .= "\t\t _gaq.push(['_trackPageview']);\n\n";		
		$analytics_js .= "\t\t (function() {\n\n";
		$analytics_js .= "\t\t\t\t var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;\n\n";
		$analytics_js .= "\t\t\t\t ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n\n";
		$analytics_js .= "\t\t\t\t var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);\n\n";
		$analytics_js .= "\t\t })();\n\n";
		$analytics_js .= "\t\t <!-- Google Analytics Plugin by PB Web Development -->\n";			
		
		if($display == '1') {		
		
			$code = '<script type="text/javascript">';
			$code .= ''.$analytics_js.'';	
			$code .= '</script>';	
			
			//footer display
			$buffer = JResponse::getBody();
			
				$buffer = preg_replace("/<\/head>/", "\n\n ".$code." \n\n</head>", $buffer);
			
			//output the buffer
			JResponse::setBody($buffer);
			
			return true;
			
		} else if($display == '0') {
			
			$code = '<script type="text/javascript">';
			$code .= ''.$analytics_js.'';	
			$code .= '</script>';	
			
			//footer display
			$buffer = JResponse::getBody();
			
				$buffer = preg_replace("/<\/footer>/", "\n\n ".$code." \n\n</footer>", $buffer);
			
			//output the buffer
			JResponse::setBody($buffer);
			
			return true;
		}
	}	
}